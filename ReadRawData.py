# CertiCAV
# Copyright (C)2021 Connected Places Catapult
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact: musicc-support@cp.catapult.org.uk

## @package ReadRawData
#
# This package is for debugging/visualisation use for the user when they want to read and display the binary CertiTRACE information saved to file from the simulation runs
import glob
import sys
sys.path.insert(1,"./pyproto/")
import certitrace_pb2
import traceback

## This method opens the file passed into it via argument and prints all of the data to console
def Main():
    try:    
        # Read from file
        simulationDataString = open(sys.argv[1],"rb")
        rawScenarioData = certitrace_pb2.SimulationRecords()
        rawScenarioData.ParseFromString(simulationDataString.read())

        file= open(rawScenarioData.concreteScenarioIdentifier + ".txt","w")
        file.write(rawScenarioData.__str__())
        file.close()

        # Writes Data to console
        print(rawScenarioData.__str__())
        print("Header Data : \n")
        print("\nGrouping scenario identifier : \n\t",rawScenarioData.groupingScenarioIdentifier)
        print("\nGrouping scenario description : \n\t",rawScenarioData.groupingScenarioDescription)
        print("\nConcrete scenario identifier : \n\t",rawScenarioData.concreteScenarioIdentifier)
        print("\nAdditional run audit data : \n\n",rawScenarioData.additionalRunAuditData)
        print("\nStatic simulation information : \n\n",rawScenarioData.staticSimulationInformation.__str__())

    except Exception as e:
        traceback.print_exc()

if __name__ == "__main__":
    Main()    
            


    