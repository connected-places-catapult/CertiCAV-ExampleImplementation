# Project Overview

CertiCAV was a project which aimed to identify principles and generate evidence relating to the type approval and safety assurance of Highly Automated Vehicles (HAVs). The project only considered the safety of the perception and tactical decision making of a nominally performing HAV (i.e. it does not attempt to address operational safety or component failures).
 
This repository contains source code for a software framework developed as part of the CertiCAV project. It represents a proof-of-concept implementation of automated testing methods for HAV assessment, applied in a way which is compatible with the wider CertiCAV framework (which is currently unpublished). Developing it was intended to generate knowledge about how to practically apply this type of testing, to inform both the rest of the CertiCAV project and any future work based on its outputs.
 
The software was developed by Connected Places Catapult. A [report describing its architecture and learnings](https://cp.catapult.org.uk/news/implementing-and-evaluating-safety-metrics-for-cavs-report/) for HAV verification is available.

# Example Implementation

This repository is for an example implementation of the core framework CertiCAV found at https://gitlab.com/connected-places-catapult/CertiCAV

This example implementation contains all the files from the core framework CertiCAV, with the addition of the implementation of MUSICC as the scenario database, CARLA as the Simulator and [scenario_runner](https://github.com/carla-simulator/scenario_runner) for running the scenarios in CARLA.

If you wish to create your own implementation with a different Simulator/Scenario Database or both then we welcome you to fork the [core framework](https://gitlab.com/connected-places-catapult/CertiCAV) (as we have done for this repo) and get coding.

This example implementation was developed on Linux, and does not fully work on Windows (see [Windows Notes](#windows-notes))

# Quick Start Guide

## Scenario_Runner submodule

scenario_runner is already included in this repo it just needs to be initialised and updated:

1. Open a terminal and change directory to the repo directory (/CertiCAV/) 

2. Run `git submodule update --init`, this will initialise and update the submodule making scenario_runner usable once the python requirements are installed. 

## Python

1. Install Python 3.7 (https://www.python.org/downloads/)

2. If you have multiple versions of Python on your machine ensure version 3.7 is the one you are using (this can be done using system paths, virtual environments, calling the directory...etc).

3. Open terminal and navigate to the repo's directory (/CertiCAV/).

4. Ensure you are using python 3.7 by running `python --version`

5. Install the dependencies by running `pip install -r requirements.txt`

## CARLA (The simulator used for the example implementation)

CARLA version 0.9.10 is required by the CertiCAV software. To install, follow the guide below to get to a point where you have a CARLAUE4.sh executable.

https://carla.readthedocs.io/en/latest/start_quickstart/

Once Carla is installed, set the environmental variable CARLA_ROOT to the directory of CarlaUE4.sh.

## CertiTRACE

CertiTRACE is a protobuf trace format made up of a proto file that references a slightly altered version of the ASAM OSI proto files (https://github.com/OpenSimulationInterface).

These are already compiled and included in the Repository but if you wish to rebuild follow the instructions below:

To compile these proto files you need to :

1. Open terminal and change directory to the repo's directory (/CertiCAV/).

2. Run the following command in the terminal `./Compile_Protos.sh` 

## MUSICC (The scenario database used for the example implementation)

1. Go to https://musicc.cp-catapult.org.uk/accounts/login/

2. Click "Sign up".

3. Fill in the details on the page and click "Sign up".

4. Once you have an account you need to fill in those Musicc details into the Config.json data in the fields (ScenarioDatabaseCredentials -> Username and Password).

## Windows Notes

The software does not currently work correctly on Windows, due to a bug with CARLA 0.9.10 (https://github.com/carla-simulator/carla/issues/3119).
1. Once CARLA is installed, ensure you set the PYTHONPATH environment variable as documented in the [Scenario Runner quick-start](https://carla-scenariorunner.readthedocs.io/en/latest/getting_started/).
2. A binary version of the Geos library is needed for the Python dependency Shapely. The simplest way of getting this is to install Shapely 1.7.1 from https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely (and updating the requirements.txt for this repo to have Shapely==1.7.1)
3. CARLA 0.9.11 may work; however, our version of scenario_runner is forked from the 0.9.10-compatible branch.

# How To Run

Once you have everything installed, open up the config.json file and fill in the fields to configure the program to your liking.
At this point you're ready to run CertiCAV:
1. Open up CMD/Terminal in the CertiCAV folder
2. Run "Python CertiCAV-Master.py"
That's it! You're now running CertiCAV and can navigate yourself through the command line menus to perform your required tasks
3. This software has been tested with the MUSICC scenario "Challenger pulls out in front of Ego (Logical)", i.e. query string `label = "Challenger pulls out in front of Ego (Logical)"`

# Documentation

The documentation for the project is compiled of a combination of in-code comments and auto generated doxygen documentation found in the repo under /CertiCAV/Documentation/ 

The Main Page being found at /CertiCAV/Documentation/html/index.html

# Useful Links

MUSICC (Scenario Database): https://cp.catapult.org.uk/project/multi-user-scenario-catalogue-for-connected-and-autonomous-vehicles/

CARLA (Simulator): https://carla.org/

scenario_runner (Scenario runner for Carla): https://github.com/carla-simulator/scenario_runner

OpenSCENARIO (Scenario Descriptor): https://www.asam.net/standards/detail/openscenario/

OpenDRIVE (Scenario Road Network Descriptor): https://www.asam.net/standards/detail/opendrive/

OSI (Base of CertiTRACE, i.e ground truth): https://www.asam.net/standards/detail/osi/

# Who do I talk to? 

Support -  musicc-support@cp.catapult.org.uk

Repo Owner - Sam Nichols (Sam.Nichols@cp.catapult.org.uk)

Moderator - Robert Myers (Robert.Myers@cp.catapult.org.uk)

Moderator - Zeyn Saigol (Zeyn.Saigol@cp.catapult.org.uk)

