# CertiCAV
# Copyright (C)2021 Connected Places Catapult
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact: musicc-support@cp.catapult.org.uk

## @package TestScripts
#
# This package is to encapsulate the test scripts (which are used to analyse the performance of the ADS/Human in the scenarios)
import math
import shapely
import time
import traceback
import json
import os
from enum import Enum

## Class used to encode the python object to JSON String
class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj,'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)


## Class to encapsulate the test results of all test on all scenarios
class FullTestsReport:
    def __init__(self):
        self.name = ""
        self.testTypes = []
        self.resultDict = {
            "Unknown" : 0,
            "Pass" : 1,
            "Human Trials Required" : 2,
            "Non-Compliant" : 3
        }
        
    ## Adds an instance of the object TestType to the current FullTestsReport
    def addTestResult(self,testResults):
        testTypeID = -1
        for testType in testResults.testTypes:
            for itr in range(0,len(self.testTypes)):
                if self.testTypes[itr].name == testType.name:
                    testTypeID = itr
                    break
            if testTypeID == -1:
                self.testTypes.append(FullTestsReport.TestType())
                testTypeID = len(self.testTypes) - 1
                self.testTypes[testTypeID].name = testResults.testTypes[0].name
            
            for test in testType.tests:
                self.testTypes[testTypeID].addTest(test)

    ## Fills the result field dependant on its lower objects results
    def fillResultFields(self):
        for testType in self.testTypes:
            testType.fillResultFields(self.resultDict)
    
    ## Used in the encoding of the object
    def reprJSON(self):
        return dict(name=self.name, testTypes=self.testTypes)
        
    ## Class to encapsulate the test types of all test on all scenarios
    class TestType:
        def __init__(self):
            self.name = ""
            self.result = "Unknown"
            self.tests = []

        ## Adds an instance of the object Test to the current TestType
        def addTest(self,test):
            testID = -1
            for itr in range(0,len(self.tests)):
                if self.tests[itr].name == test.name:
                    testID = itr
                    break
            if testID == -1:
                self.tests.append(FullTestsReport.TestType.Test())
                testID = len(self.tests) - 1
                self.tests[testID].name = test.name
            for groupedScenario in test.groupingScenarios:
                self.tests[testID].addGroupedScenario(groupedScenario)

        ## Fills the result field dependant on its lower objects results
        def fillResultFields(self, resultDict):
            for test in self.tests:
                test.fillResultFields(resultDict)
                if resultDict[test.result] > resultDict[self.result]:
                            self.result = test.result

        ## Used in the encoding of the object
        def reprJSON(self):
            return dict(name=self.name,result=self.result, tests=self.tests) 
        
        ## Class to encapsulate the tests on all scenarios
        class Test:
            def __init__(self):
                self.name = ""
                self.result = "Unknown"
                self.groupingScenarios = []

            ## Adds an instance of the object GroupingScenario to the current FullTestsReport
            def addGroupedScenario(self,groupedScenario):
                groupedScenarioID = -1
                for itr in range(0,len(self.groupingScenarios)):
                    if self.groupingScenarios[itr].name == groupedScenario.name:
                        groupedScenarioID = itr
                        break
                if groupedScenarioID == -1:
                    self.groupingScenarios.append(FullTestsReport.TestType.Test.GroupingScenario())
                    groupedScenarioID = len(self.groupingScenarios) - 1
                    self.groupingScenarios[groupedScenarioID].name = groupedScenario.name
                for scenario in groupedScenario.scenarios:
                    self.groupingScenarios[groupedScenarioID].addScenario(scenario)

            ## Fills the result field dependant on its lower objects results
            def fillResultFields(self, resultDict):
                for groupedScenario in self.groupingScenarios:
                    groupedScenario.fillResultFields(resultDict)
                    if resultDict[groupedScenario.result] > resultDict[self.result]:
                            self.result = groupedScenario.result

            ## Used in the encoding of the object
            def reprJSON(self):
                return dict(name=self.name, result=self.result, groupingScenarios=self.groupingScenarios) 

            ## Class to encapsulate the grouping scenario on all scenarios
            class GroupingScenario:
                def __init__(self):
                    self.name = ""
                    self.result = "Unknown"
                    self.scenarios = []

                ## Adds an instance of the object Scenario to the current GroupingScenario
                def addScenario(self,scenario):
                    scenarioID = -1
                    for itr in range(0,len(self.scenarios)):
                        if self.scenarios[itr].name == scenario.name:
                            scenarioID = itr
                            print("Test results have already been allocated for this scenario")
                    if scenarioID == -1:
                        self.scenarios.append(FullTestsReport.TestType().Test.GroupingScenario.Scenario())
                        scenarioID = len(self.scenarios) - 1
                        self.scenarios[scenarioID].id = scenarioID
                        self.scenarios[scenarioID].name = scenario.name
                        self.scenarios[scenarioID].result = scenario.result
                        self.scenarios[scenarioID].score = scenario.score
                        self.scenarios[scenarioID].message = scenario.message
                    return None
                
                ## Fills the result field dependant on its lower objects results
                def fillResultFields(self,resultDict):
                    for scenario in self.scenarios:
                        if resultDict[scenario.result] > resultDict[self.result]:
                            self.result = scenario.result

                ## Used in the encoding of the object
                def reprJSON(self):
                    return dict(name=self.name, result=self.result, scenarios=self.scenarios) 

                ## Class to encapsulate a scenario
                class Scenario:
                    def __init__(self):
                        self.name = ""
                        self.result = "Unknown"
                        self.score = "N/A"
                        self.message = ""
                    
                    ## Used in the encoding of the object
                    def reprJSON(self):
                        return dict(name=self.name, result=self.result, score=self.score, message=self.message) 

## This method is used to run all of the tests on all of the scenarios passed into it and from those results, create a test results file 
#
# @param scenarios The List compiling of each CertiTRACE output from the scenarios executed
# @param queryResultDirectory The output directory from this run of CertiCAV-Master.py
# @param human Boolean value determining if the scenario outputs were from a human or ADS (different TestResult output filenames)
def RunTests(scenarios, queryResultDirectory, human = False):
    try:    
        fullTestsReport = FullTestsReport()
        fullTestsReport.name = "Test Report"
        for scenario in scenarios:
            #TODO: To add test : Write Test Method - > fullTestsReport.addTestResult({YourTest(scenario)})
            fullTestsReport.addTestResult(SpeedTest(scenario))
            fullTestsReport.addTestResult(PullOutTest(scenario))
            fullTestsReport.addTestResult(SeverityTest(scenario))

        fullTestsReport.fillResultFields()
        if human:
            humanItr = 1
            while os.path.exists(queryResultDirectory +  "/" + "TestResults(Human_{0}).txt".format(humanItr)):
                humanItr += 1
            fullTestReportFile = open(queryResultDirectory +  "/" + "TestResults(Human_{0}).txt".format(humanItr), "w")
        else:
            fullTestReportFile = open(queryResultDirectory +  "/" + "TestResults.txt", "w")
        fullTestReportFile.write(json.dumps(fullTestsReport.reprJSON(), cls=ComplexEncoder))
        fullTestReportFile.close()
        
        return fullTestsReport
    except Exception as e:
        traceback.print_exc()

## This method initialises an instance of FullTestsReport for one of the tests to fill in.
#
# Initialises an instance of FullTestsReport with test type name, test name and scenario name filled in, for use in populating the results from a singular test that is then added back into the collective FullTestsReport using the method FullTestsReport.addTestResult()
#
# @param testTypeName The test type name of the test that this FullTestsReport instance is going to be filled in by
# @param testName The name of the test that this FullTestsReport instance is going to be filled in by
# @param scenario The CertiTRACE output from one of the scenarios
def InitialiseTestResultVariable(testTypeName,testName,scenario):
    test = FullTestsReport()
    test.testTypes.append(FullTestsReport.TestType())
    test.testTypes[0].tests.append(FullTestsReport.TestType.Test())
    test.testTypes[0].tests[0].groupingScenarios.append(FullTestsReport.TestType.Test.GroupingScenario())
    test.testTypes[0].tests[0].groupingScenarios[0].scenarios.append(FullTestsReport.TestType.Test.GroupingScenario.Scenario())
    
    test.testTypes[0].name = testTypeName
    test.testTypes[0].tests[0].name = testName
    test.testTypes[0].tests[0].groupingScenarios[0].name = scenario.groupingScenarioIdentifier
    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name = scenario.concreteScenarioIdentifier

    return test


## This method checks if the ego at any point went over the speed limit
#
# @param scenario The CertiTRACE output from one of the scenarios
def SpeedTest(scenario):
    test = InitialiseTestResultVariable("Co-operative Driving Specification Tests", "Speeding Test",scenario)
    speedLimit = 70
    maxSpeed = 0
    count = 0
    for step in scenario.groundTruthSeries:
        count += 1
        print ("Running Speed Test on {0}... {1}%".format(test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name, str(int((count / len(scenario.groundTruthSeries)) * 100))), end="\r")
        for movingObject in step.moving_object:
            if movingObject.id.value == scenario.staticSimulationInformation.host_vehicle_id.value:
                mph = math.sqrt(movingObject.base.velocity.x ** 2 + movingObject.base.velocity.y ** 2) * 2.23694
                if mph > speedLimit:
                    print("")
                    print("Ego Speed : {0}".format(str(int(math.sqrt(movingObject.base.velocity.x ** 2 + movingObject.base.velocity.y ** 2) * 2.23694))),end="\033[F")
                    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Non-Compliant"
                if mph > maxSpeed:
                    maxSpeed = mph
    
    if test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result != "Non-Compliant":
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Pass"


    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "Ego Max Speed (MPH) : {0:.2f} / Speed Limit (MPH) : {1:.2f}".format(maxSpeed,speedLimit)
    print("")
    print("")
    return test

## This method returns the conflict area and priority areas of a map (This is a "dummy method" as it only returns hard coded values)   
#
# @param scenario The CertiTRACE output from one of the scenarios used to get the ODR from the scenario executed
def GetConflictAreas(scenario):
    # PREDEFINED FOR M112.xosc
    conflictArea1 = [[-4.35,-0.07],[-0.35,-0.07],[-0.35,-12.47],[-4.35,-12.47]]
    priorityArea1 = [[-4.35,2.07],[-0.35,2.07],[-0.35,-0.07],[-4.35,-0.07]]

    conflictArea2 = [[-0.35,-0.07],[3.65,-0.07],[3.65,-12.47],[-0.35,-12.47]]
    priorityArea2 = [[-0.35,-12.47],[3.65,-12.47],[3.65,-14.47],[-0.35,-14.47]]

    conflictAreas = [[conflictArea1,priorityArea1],[conflictArea2,priorityArea2]]
    return conflictAreas

## Checks if an object is intersecting with a conflict area
#
# @param movingObject The object - used to get positional data and orientational data
# @param bb The bounding box of the object referenced in movingObject
# @param zone The co-ordinates of the corner points of the conflict area
def IsIntersecting(movingObject, bb, zone):
    actorCorners = GetCorners(movingObject,bb)# find all corners of the actor
    actorPoly = shapely.geometry.Polygon(actorCorners)
    conflictAreaPoly = shapely.geometry.Polygon(zone)
    if actorPoly.intersects(conflictAreaPoly):
        return True
    else:
        return False

## Finds the closest distance of an object to the conflict area
#
# @param bbCorners the co-ordinates of the corners of an object
# @oaram zone The co-ordinates of the corners of the conflict area
def GetClosesetDistance(bbCorners,zone):
    closestDistance = 999999999999
    for bbCorner in bbCorners:
        for zoneCorner in zone:
            distance = math.sqrt((zoneCorner[0] - bbCorner[0]) ** 2 + (zoneCorner[1] - bbCorner[1]) ** 2)
            if distance < closestDistance:
                closestDistance = distance
    
    return closestDistance

## Gets the co-ordinates of the corners of an object
#
# @param vehicle The positional and orientation of an object from its center point
# @param bb The bounding box of the object referenced in vehicle
def GetCorners(vehicle,bb):
    yaw = math.radians(vehicle.base.orientation.yaw + 90)
    halfWidth = bb.width / 2
    halfLength = bb.length / 2

    vehicleX = vehicle.base.position.x
    vehicleY = vehicle.base.position.y

    #print("Yaw (Deg) : ", vehicle.base.orientation.yaw)
    #print("Yaw : ", yaw)
    
    
    frontLeft = [vehicleX + (halfLength * math.sin(yaw) - halfWidth * math.cos(yaw)),vehicleY + (halfLength * math.cos(yaw) + halfWidth * math.sin(yaw))]
    frontRight = [vehicleX + (halfLength * math.sin(yaw) + halfWidth * math.cos(yaw)),vehicleY + (halfLength * math.cos(yaw) - halfWidth * math.sin(yaw))]
    backRight = [vehicleX + (-halfLength * math.sin(yaw) + halfWidth * math.cos(yaw)),vehicleY + (-halfLength * math.cos(yaw) - halfWidth * math.sin(yaw))]
    backLeft = [vehicleX + (-halfLength * math.sin(yaw) - halfWidth * math.cos(yaw)),vehicleY + (-halfLength * math.cos(yaw) + halfWidth * math.sin(yaw))]

    corners = [frontLeft,frontRight,backRight,backLeft]
    return corners

## Tests who is at fault in a scenario if there is a collision during an T-Junction pullout event
#
# Read the in document comments for a higher understanding of the test itself
#
# @param scenario The CertiTRACE output from one of the scenarios
def PullOutTest(scenario):
    try:
        test = InitialiseTestResultVariable("Collision Responsibility Specification Tests", "Pull Out Safety Test",scenario)
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Pass"

        print("Running Pull Out Test on {0}...".format(test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name))

        # Check for conflict areas in scenario
        conflictAreas = GetConflictAreas(scenario)
        if len(conflictAreas) > 0:
            # Setting Recall step to -1 as no information is important to us as of yet
            recallFromThisStep = -1
            # Check for collisions in scenario
            for stepID in range(0,len(scenario.groundTruthSeries)):
                if len(scenario.groundTruthSeries[stepID].collisions) > 0:
                    recallFromThisStep = stepID
                    egoID = scenario.groundTruthSeries[recallFromThisStep].collisions[0].actors[0].id.value
                    collisionActorID = scenario.groundTruthSeries[recallFromThisStep].collisions[0].actors[1].id.value
                    break

            #If there wasnt a collision then pass the ego for this test
            if recallFromThisStep == -1:
                print("Running Pull Out Test on {0}... Completed No Collision".format(test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name))
                test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "No Collision"
                return test

            bbDict = {}
            # Make BoundingBox dictionary by ID
            for movingObject in scenario.staticSimulationInformation.moving_object:
                bbDict[movingObject.id.value] = movingObject.base.dimension

            # Get All Conflict Areas Passed through before point of collision

            goToNextConflictArea = False
            # Check for actor intersecting with any conflict area
            for stepID in range(recallFromThisStep,0,-1):
                #print(stepID)
                for movingObject in scenario.groundTruthSeries[stepID].moving_object:
                    if movingObject.id.value == collisionActorID or movingObject.id.value == egoID:
                        #print("x: ", movingObject.base.position.x, " / y: ", movingObject.base.position.y)
                        for conflictAreaID in range(0,len(conflictAreas)):
                            #print("")
                            #print("Checking Intersection with")
                           
                            if IsIntersecting(movingObject, bbDict[movingObject.id.value], conflictAreas[conflictAreaID][0]):
                                print("Vehicle (id:{0}) intersected with conflict area (id:{1}) at step {2}".format(str(movingObject.id.value),str(conflictAreaID),str(stepID)))
                                #print(GetCorners(movingObject,bbDict[movingObject.id.value]))
                                
                                # Assigns licaID and otherActorID to their related vehicle IDs
                                if (movingObject.id.value == egoID):
                                    latestInConflictAreaID = egoID
                                    otherActorID = collisionActorID
                                else:
                                    latestInConflictAreaID = collisionActorID
                                    otherActorID = egoID

                                # Check for otherActor intersecting with any conflict area up to the current step of the LICA (Latest in conflict area) Actor
                                for otherActorStep in range(recallFromThisStep,stepID,-1):
                                    for movingObject2 in scenario.groundTruthSeries[otherActorStep].moving_object:
                                        if movingObject2.id.value == otherActorID:
                                            if IsIntersecting(movingObject2, bbDict[movingObject.id.value], conflictAreas[conflictAreaID][0]):
                                                print("Other vehicle (id:{0}) also intersected with conflict area (id:{1}) at step {2}".format(str(movingObject2.id.value),str(conflictAreaID),str(otherActorStep)))

                                                otherActorInPriorityLane = None
                                                licaActorInPriorityLane = None
                                                # Check if ego is in a priority or a non priority lanes
                                                for otherActorBacktrackStep in range(otherActorStep,0,-1):
                                                    for movingObject3 in scenario.groundTruthSeries[otherActorBacktrackStep].moving_object:
                                                        if movingObject3.id.value == otherActorID:
                                                            if not IsIntersecting(movingObject3, bbDict[movingObject3.id.value], conflictAreas[conflictAreaID][0]):
                                                                print("Other vehicle (id:{0}) is no longer in the conflict area at step {1}".format(str(movingObject3.id.value),str(otherActorBacktrackStep)))
                                                                if IsIntersecting(movingObject3, bbDict[movingObject3.id.value], conflictAreas[conflictAreaID][1]):
                                                                    print("Other vehicle (id:{0}) is from the priority lane".format(str(movingObject3.id.value)))
                                                                    otherActorInPriorityLane = True
                                                                else:
                                                                    print("Other vehicle (id:{0}) is from the non-priority lane".format(str(movingObject3.id.value)))
                                                                    otherActorInPriorityLane = False
                                                    if otherActorInPriorityLane != None:
                                                        break

                                                # Check if actor is in a priority or a non priority lanes
                                                for licaActorBacktrackStep in range(stepID,0,-1):
                                                    for movingObject3 in scenario.groundTruthSeries[licaActorBacktrackStep].moving_object:
                                                        if movingObject3.id.value == latestInConflictAreaID:
                                                            if not IsIntersecting(movingObject3, bbDict[movingObject3.id.value], conflictAreas[conflictAreaID][0]):
                                                                print("LICA vehicle (id:{0}) is no longer in the conflict area at step {1}".format(str(movingObject3.id.value),str(licaActorBacktrackStep)))
                                                                if IsIntersecting(movingObject3, bbDict[movingObject3.id.value], conflictAreas[conflictAreaID][1]):
                                                                    print("LICA vehicle (id:{0}) is from the priority lane".format(str(movingObject3.id.value)))
                                                                    licaActorInPriorityLane = True
                                                                else:
                                                                    print("LICA vehicle (id:{0}) is from the non-priority lane".format(str(movingObject3.id.value)))
                                                                    licaActorInPriorityLane = False
                                                    if licaActorInPriorityLane != None:
                                                        break
                                                
                                                if licaActorInPriorityLane == otherActorInPriorityLane:
                                                    # Both are in priority or non priority lane meaning the offense could have happened before this conflict point so continue
                                                    goToNextConflictArea = True
                                                    conflictAreas.pop(conflictAreaID)
                                                elif licaActorInPriorityLane != otherActorInPriorityLane:
                                                    # one is pulling out and the other is in priority lane meaning we have a scenario which we can analyse and perform tests on

                                                    if licaActorInPriorityLane:
                                                        pulloutVehicleID = otherActorID
                                                        mainroadVehicleID = latestInConflictAreaID
                                                        pulloutStartStep = otherActorStep

                                                    else:
                                                        pulloutVehicleID = latestInConflictAreaID
                                                        mainroadVehicleID = otherActorID
                                                        pulloutStartStep = stepID
                                                    
                                                    for pulloutBacktrackStep in range(pulloutStartStep,0,-1):
                                                        for movingObject3 in scenario.groundTruthSeries[pulloutBacktrackStep].moving_object:
                                                            if movingObject3.id.value == pulloutVehicleID:
                                                                if not IsIntersecting(movingObject3, bbDict[movingObject3.id.value], conflictAreas[conflictAreaID][0]):
                                                                    print("The pullout vehicle (id:{0}) started the pullout at step {1}".format(str(pulloutVehicleID),str(pulloutBacktrackStep + 1)))
                                                                    # The actor is no longer intersecting with the conflict area so perform tests at currentStep + 1
                                                                    for movingObject4 in scenario.groundTruthSeries[pulloutBacktrackStep + 1].moving_object:
                                                                        if movingObject4.id.value == mainroadVehicleID:

                                                                            # Calculate the distance between the ego and the closest point of the conflict area
                                                                            closestDistance = GetClosesetDistance(GetCorners(movingObject4,bbDict[movingObject4.id.value]),conflictAreas[conflictAreaID][0])
                                                                            # Calculate the SSD (vt + v^(2)) / 2d  where v = MIN(speed (m/s) or Speed limit), t = driver perception-reaction time (2 for ads), d = deceleration (m/s) (2.45 for ads))
                                                                            v = math.sqrt(movingObject4.base.velocity.x ** 2 + movingObject4.base.velocity.y ** 2)
                                                                            t = 2
                                                                            d = 2.45
                                                                            
                                                                            ssd = (v * t) + (v**2) / (2 * d)
                                                                            # Check if actor distance is < SSD 
                                                                            if closestDistance <= ssd:
                                                                                if pulloutVehicleID == egoID:
                                                                                    # Non-Compliant ego for pulling in an unsafe way
                                                                                    print("Non-Compliant")
                                                                                    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Non-Compliant"
                                                                                    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "At ego pullout, actor is {0:.2f}m away from the conflict area when safe stopping distance is {1:.2f}m".format(closestDistance, ssd)
                                                                                else:
                                                                                    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "At actor pullout, ego is {0:.2f}m away from the conflict area when safe stopping distance is {1:.2f}m".format(closestDistance, ssd)
                                                                            elif closestDistance > ssd:
                                                                                if mainroadVehicleID == egoID:
                                                                                    print("Non-Compliant")
                                                                                    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Non-Compliant"
                                                                                    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "At actor pullout, ego is {0:.2f}m away from the conflict area when safe stopping distance is {1:.2f}m".format(closestDistance, ssd)
                                                                                else:
                                                                                    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "At ego pullout, actor is {0:.2f}m away from the conflict area when safe stopping distance is {1:.2f}m".format(closestDistance, ssd)

                                                                            print("Running Pull Out Test on {0}... Completed".format(test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name))
                                                                            return test
                                    if goToNextConflictArea == True:
                                        break
                            if goToNextConflictArea == True:
                                goToNextConflictArea = False
                                break
        print("Running Pull Out Test on {0}... Completed".format(test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name))
        return test
    except Exception as e:
        traceback.print_exc()

## Tests the severity of the first collision in the scenario
#
# To calculate the severity the calculation used is the square of the velocity change at the step before and after the collision + half of the remaining velocity 
#
# @param scenario The CertiTRACE output from one of the scenarios
def SeverityTest(scenario):

    test = InitialiseTestResultVariable("Reference Model Comparison Tests", "Severity Test",scenario)

    print("Running Severity Test on {0}...".format(test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name), end="\r")    

    collisionStep = -1
    for stepID in range(0,len(scenario.groundTruthSeries)):
                if len(scenario.groundTruthSeries[stepID].collisions) > 0:
                    collisionStep = stepID
                    egoID = scenario.groundTruthSeries[collisionStep].collisions[0].actors[0].id.value
                    collisionActorID = scenario.groundTruthSeries[collisionStep].collisions[0].actors[1].id.value
                    break
    
    if collisionStep == -1:
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Pass"
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].score = str(0)
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "No Collision"
        return test

    for movingObject in scenario.groundTruthSeries[collisionStep - 1].moving_object:
        if movingObject.id.value == egoID:
            egoBefore = movingObject.base.velocity
        if movingObject.id.value == collisionActorID:
            collisionActorBefore = movingObject.base.velocity

    for movingObject in scenario.groundTruthSeries[collisionStep + 1].moving_object:
        if movingObject.id.value == egoID:
            egoAfter = movingObject.base.velocity
        if movingObject.id.value == collisionActorID:
            collisionActorAfter = movingObject.base.velocity

    score = (egoBefore.x - egoAfter.x)**2 + (egoBefore.y - egoAfter.y)**2 + (egoBefore.z - egoAfter.z)**2 + (collisionActorBefore.x - collisionActorAfter.x)**2 + (collisionActorBefore.y - collisionActorAfter.y)**2 + (collisionActorBefore.z - collisionActorAfter.z)**2

    score += 0.5 * ((egoAfter.x - 0) + (egoAfter.y - 0) + (egoAfter.z - 0) + (collisionActorAfter.x - 0) + (collisionActorAfter.y - 0) + (collisionActorAfter.z - 0))

    if score > math.inf:
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Non-Compliant"
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].score = "{0:2f}".format(score)
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "Collision Severity was too high"
    else:
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].result = "Human Trials Required"
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].score = "{0:2f}".format(score)
        test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].message = "There was a collision but Severity was low"
    
    print("Running Severity Test on {0}...Done".format(test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name))    

    return test
