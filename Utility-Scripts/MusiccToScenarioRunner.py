# CertiCAV
# Copyright (C)2021 Connected Places Catapult
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact: musicc-support@cp.catapult.org.uk

## @package MusiccToScenarioRunner
#
# This file is part of the Example Implementation and used to convert to allow the sceanrio runner to run as intended
import xml.etree.ElementTree as ET

## This method takes the path input to the original scenario and adds to/changes the file so that it is compaitible with the sceanrio runner
def ConvertMusiccOSCToScenarioRunner(path):
    tree = ET.parse(path)
    root = tree.getroot()
    
    # Adding a description to the fileheader if not already present
    fileheader = root.find("FileHeader")
    if (fileheader == None):
        fileheaderElement = ET.Element("FileHeader",{"author": "Unknown","date": "Unknown","description": "Unknown"})
        root.insert(0,fileheaderElement)
    else:
        description = fileheader.get("description")
        if(description == None or description == ""):
            fileheader.set("description","Creation Description")

    

    # Inversing relative lane change values / ensuring lane change dynamicsShape is linear / ensuring lane change dynamicsDimension is distance / ensuring routeStrategy is always fastest 
    for storyboard in root.findall("Storyboard"):
        for story in storyboard.findall("Story"):
            for act in story.findall("Act"):
                for maneuverGroup in act.findall("ManeuverGroup"):
                    for maneuver in maneuverGroup.findall("Maneuver"):
                        for event in maneuver.findall("Event"):
                            for action in event.findall("Action"):
                                for privateAction in action.findall("PrivateAction"):
                                    for laternalAction in privateAction.findall("LateralAction"):
                                        for laneChangeAction in laternalAction.findall("LaneChangeAction"):
                                            for laneChangeActionDynamics in laneChangeAction.findall("LaneChangeActionDynamics"):
                                                if laneChangeActionDynamics.get("dynamicsShape") != "linear":
                                                    print("Converting LaneChangeActionDynamics -> dynamicsShape from {0} to linear".format(laneChangeActionDynamics.get("dynamicsShape")))
                                                    laneChangeActionDynamics.set("dynamicsShape","linear")
                                                if laneChangeActionDynamics.get("dynamicsDimension") != "distance":
                                                    print("Converting LaneChangeActionDynamics -> dynamicsDimension from {0} to distance".format(laneChangeActionDynamics.get("dynamicsDimension")))
                                                    laneChangeActionDynamics.set("dynamicsDimension","distance")
                                            for laneChangeTarget in laneChangeAction.findall("LaneChangeTarget"):
                                                for relativeTargetLane in laneChangeTarget.findall("RelativeTargetLane"):
                                                    if(relativeTargetLane.get("value")[0] != "$"):
                                                        if(relativeTargetLane.get("value")[0] == "-"):
                                                            print("Converting relativeTargetLane -> value from {0} to {1}".format(relativeTargetLane.get("value"), relativeTargetLane.get("value")[1:]))
                                                            relativeTargetLane.set("value",relativeTargetLane.get("value")[1:])
                                                        else:
                                                            print("Converting relativeTargetLane -> value from {0} to {1}".format(relativeTargetLane.get("value"), ("-" + relativeTargetLane.get("value"))))
                                                            relativeTargetLane.set("value","-" + relativeTargetLane.get("value"))
                                                    else:
                                                        for parameter in root.find("ParameterDeclarations").findall("ParameterDeclaration"):
                                                            if parameter.get("name") == relativeTargetLane.get("value")[1:]:
                                                                if(parameter.get("value")[0] == "-"):
                                                                    print("Converting relativeTargetLane -> value from Parameterised value of {0} to Non-Parameterised value of {1}".format(parameter.get("value"), parameter.get("value")[1:]))
                                                                    relativeTargetLane.set("value",parameter.get("value")[1:])
                                                                else:
                                                                    print("Converting relativeTargetLane -> value from Parameterised value of {0} to Non-Parameterised value of {1}".format(parameter.get("value"),("-" + parameter.get("value"))))
                                                                    relativeTargetLane.set("value",("-" + parameter.get("value")))
                                    for routingAction in privateAction.findall("RoutingAction"):
                                        for assigningRouteAction in routingAction.findall("AssigningRouteAction"):
                                            for route in assigningRouteAction.findall("Route"):
                                                for waypoint in assigningRouteAction.findall("Waypoint"):
                                                    if waypoint.get("routeStrategy") != "fastest":
                                                        print("Converting Waypoint -> routeStrategy from {0} to fastest".format(waypoint.get("routeStrategy")))
                                                        waypoint.set("routeStrategy","fastest")

    tree.write(path)


if __name__ == "__main__":
    Convert_Musicc_OSC_to_Scenario_Runner("/home/samnichols/OSC-ALKS-scenarios-master/Scenarios/ALKS_Scenario_4.4_2_CutInUnavoidableCollision_TEMPLATE.xosc")
