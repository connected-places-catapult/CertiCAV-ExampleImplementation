# CertiCAV
# Copyright (C)2021 Connected Places Catapult
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact: musicc-support@cp.catapult.org.uk
## @package CertiCAV
# Documentation for the file CertiCAV-Master.py

## @mainpage CertiCAV Documentation Main Page
# @section Description
# CertiCAV is a framework which allows the user to read in (a) scenario(s) from a scenario database and run them in a simulator generating CertiTRACE files with the ground truth data in them and then perform tests on them.\n
# Thus determining if the ADS's results are compliant, non-compliant of the specification or require human trials for comparison (which the CertiCAV framework also allows for)
# @section Flow 
# The programs flow depends on the users choice of what they want to do in the UI, the general flow is as follows:\n
# CertiCAV-Master.Main() -> CertiCAV.UI() followed by: \n
# If Running Scenarios (ADS): -> CertiCAV.QueryScenarioDatabase() -> CertiCAV.UI() -> CertiCAV.GetScenarioPaths() -> CertiCAV.UI() -> CertiCAV.QueryResultsToConcrete() -> CertiCAV.UI() -> CertiCAV.InitialiseSimulator() -> CertiCAV.UI() -> CertiCAV.RunScenarios() -> CertiCAV.UI() -> CertiCAV.WaitForOutputs() -> CertiCAV.UI()\n
# If Running Scenarios (Human Trials): -> CertiCAV.GetScenarioPathsFromTestResults() -> CertiCAV.UI() -> CertiCAV.InitialiseSimulator() -> CertiCAV.UI() -> CertiCAV.RunScenarios() -> CertiCAV.UI()\n
# If Running Tests (ADS): -> TestScripts.RunTests() -> CertiCAV.UI() -> CertiCAV.CheckForHumanTrialsRequired()... If False -> CertiCAV.GenerateReport()\n
# If Running Tests (Human Trials): -> TestScripts.RunTests() -> CertiCAV.UI() -> CertiCAV.CompareResultsAndProduceTestResultsFinal() -> CertiCAV.UI() -> CertiCAV.GenerateReport()\n

import sys
import time
import json
import os
import logging
import glob
import subprocess
import traceback
import zipfile
from datetime import datetime
from datetime import timedelta
from threading import Thread
from enum import Enum

sys.path.insert(1,"./Simulation-Scripts/")
sys.path.insert(1,"./Utility-Scripts/")
sys.path.insert(1,"./scenario_runner/")
sys.path.insert(1,"./pyproto/")
from MusiccToScenarioRunner import ConvertMusiccOSCToScenarioRunner
from OSCToHumanTrial import ConvertOSCToHumanTrialOSC
from TestScripts import RunTests
from query import MusiccSession
import scenario_runner
import certitrace_pb2
import psutil

## This class logs necessary program information to file 
class MyLogger(logging.Logger):

    def __init__(self, name, level = logging.NOTSET):
        return super(MyLogger, self).__init__(name, level)        
    
    def info(self, msg, *args, **kwargs):
        return super(MyLogger, self).info((datetime.now().strftime("%Y_%m_%d_(%H-%M-%S)") + " : " + msg), *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        return super(MyLogger, self).warning((datetime.now().strftime("%Y_%m_%d_(%H-%M-%S)") + " : " + msg), *args, **kwargs)

    def error(self, msg, *args, exc_info, stack_info, extra, **kwargs):
        return super(MyLogger,self).error((datetime.now().strftime("%Y_%m_%d_(%H-%M-%S)") + " : " + msg), *args, exc_info, stack_info, extra, **kwargs)

    def debug(self, msg, *args, exc_info, stack_info, extra, **kwargs):
        return super(MyLogger, self).debug((datetime.now().strftime("%Y_%m_%d_(%H-%M-%S)") + " : " + msg), *args, exc_info, stack_info, extra, **kwargs)    

    def critical(self, msg, *args, exc_info, stack_info, extra, **kwargs):
        return super(MyLogger, self).critical((datetime.now().strftime("%Y_%m_%d_(%H-%M-%S)") + " : " + msg), *args, exc_info, stack_info, extra, **kwargs)
    
## This class is used for encapsulating the configuration information (read in from the config file)
class ConfigData():

    def __init__(self,config):

        try:
            self.url = config['ScenarioDatabaseCredentials']['url'] ### The url for Musicc
            		
            self.username = config['ScenarioDatabaseCredentials']['username']### The username the user has for their Musicc credentials

            self.password = config['ScenarioDatabaseCredentials']['password'] ### The password the user has for their Musicc credentials

            self.querySource = config['QueryStringDirectory'] ### The directory where the user is keeping all of the static query string files

            self.outputDirectory = config['CertiTRACEOutputDirectory'] ### The directory which we want all of our outputs from the program to be.
            
            self.organisation = config['Organisation'] ### The organisation using CertiCAV and thus producing the output files

        except:
            raise Exception("Error in Config Read\nConfig currently reads as {0}".format(vars(self)))

## This class is where all of the main operations happen in CertiCAV (UI, Scenario database query and download, Initialise Simulator, Run Scenarios, Generate Report)
class CertiCAV:
    ## Init gets all of the classes variables initialised including the logger and configData objects
    #
    # @param logr The MyLogger instance that we want to use as a logger for this class
    # @param configDta The ConfigData instance that has been read in from file
    def __init__(self, logr, configDta):
        self.logger = logr  ### The MyLogger instance that is user for logging information to file
        self.configData = configDta ### The ConfigData instance used for storing and accessing the configuration data
        self.queryString = "" ### The query string that is used to query the scenario database
        self.queryResults = [] ### The results returned from the scenario database query
        self.queryResultsConcrete = [] ### The results returned from the scenario database query extended to have a field for each concrete scenario
        self.queryResultsDirectory = "" ### The directory for the scenarios that you are concerned with during this run (this could be the ones downloaded or ones specified if running human trials)
        self.timeoutTime = None ### The time at which the master script should stop waiting for the scenarios to be finished
        self.concreteScenariosPerLogical = 5 ### The number of concrete scenarios we want to extract from each logical scenario
        self.scenarioPaths = [] ### The paths to the scenarios you are concerned with during this run
        self.outputFileNames = [] ### The names of the files that you want to output
        self.queryUrl = "" ### The url for the scenario database's query page
        self.certicavCommit = "" ### the CertiCAV repos commit 
        
    ## Method that defines and runs through the User Interface
    def UI(self):
        ##This enum is for the task that the user wants to perform
        class UIChoice(Enum):
            ADSRunScenariosAndTests = 1
            ADSRunScenarios = 2
            ReadInAndRunTests = 3
            HumanTrials = 4

        # Getting user input
        repeat = True
        while(repeat):
            self.logger.info("Requesting user input for which task to perform...")

            userStartPoint = input("*********************************************************************************************\n\nWelcome To CertiCAV\n\nEnter the number which is next to your desired choice\n\n\t1 <--- Run Scenarios, Output Raw Data Files and Run Tests on those Outputs\n\t2 <--- Run Scenarios and Output Raw Data Files\n\t3 <--- Read Raw Data Files and Run Tests on them\n\t4 <--- Run Human Trials\n\n")

            # Checking for valid input
            if (userStartPoint == "1" or userStartPoint == "2" or userStartPoint == "3" or userStartPoint == "4"):
                userStartPoint = UIChoice(int(userStartPoint))
                repeat = False

        # Starting The Run Scenario and Output Part of the Program
        if(userStartPoint == UIChoice.ADSRunScenariosAndTests or userStartPoint == UIChoice.ADSRunScenarios):
            self.logger.info("Starting process to run scenarios")

            # Getting User input to select import query string or write query string
            repeat = True
            while(repeat):
                self.logger.info("Requesting user input for writing or importing query string for MUSICC API Call...")
                writeQueryString = input("*****Musicc Query String*****\n\nEnter the number which is next to your desired choice\n\n\t1 <--- Import Query String from File\n\t2 <--- Write your own Query String\n\n")

                # Checking for valid input
                if (writeQueryString == "1" or writeQueryString == "2"):
                    writeQueryString = int(writeQueryString) - 1
                    repeat = False

            if(writeQueryString):
                self.logger.info("Requesting users query string...")
                    
                repeat = True
                while(repeat):

                    # Getting User's MUSICC Query String
                    self.queryString = input("\nPlease Enter Your Desired Query String\n\n") 

                    #TODO:Perform Validity Check on Users Query String
                    if(self.queryString != "Incorrect"):
                        repeat = False
                        self.logger.info("Query String Written : {}".format(self.queryString))

            else:
                repeat = True
                while(repeat):
                    self.logger.info("Requesting user input for choosing which query string file to import...")
                    # Display all files in query directory in config
                    files = os.listdir(self.configData.querySource)
                    textFiles = []
                    for file in files:
                        if (file[len(file)-4:len(file)]== ".txt"):
                            textFiles.append(file)
                    textFiles.sort()

                    self.logger.info("Query string files output to user for selection : {}".format(textFiles))

                    # Getting user input for which file to inport from
                    print("Enter the number which is next to your desired choice")
                    for index,textFile in enumerate(textFiles):
                        print("\t{0} <--- {1}".format(index+1, textFile))
                    queryFileSelected = input()

                    # Checking for valid response
                    try:
                        queryFileSelected = int(queryFileSelected) - 1
                    except:
                        print("Invalid Input")
                        continue
                    if(queryFileSelected < len(textFiles)):
                        repeat = False

                self.logger.info("Query string file selected : {}".format(self.configData.querySource + textFiles[queryFileSelected]))
                with open(self.configData.querySource + textFiles[queryFileSelected], 'r') as file:
                    self.queryString = file.read().replace('\n', '')
                    print("Query String imported : {0}\n".format(self.queryString))

            self.logger.info("Making API Call to MUSICC with following credentials : url={0}, username={1}, password={2}".format(self.configData.url,self.configData.username,self.configData.password))

            # Queries Musicc with self.queryString
            self.QueryScenarioDatabase()
        
            # Gets all Scenario Paths
            self.GetScenarioPaths()

            # Creates a result set with one result for each concrete requested
            self.QueryResultsToConcrete()

            # Prints the Result set Concrete
            scenarioNum = 0
            for result in self.queryResultsConcrete:
                scenarioNum += 1
                print("Scenario {0} : Musicc ID {1} : Concrete ID {2} : Label {3}".format(scenarioNum,result['id'] ,os.path.basename(self.scenarioPaths[scenarioNum - 1]) , result['metadata']['label']))

            # Get Simulation Scripts start time
            self.logger.info("Calling Simulation Startup Scripts")
            self.timeoutTime = datetime.now() + timedelta(minutes=5)  
            
            self.InitialiseSimulator()

            # Get CertiCAV commit
            self.certicavCommit = subprocess.check_output(["git", "describe", "--always"]).strip()

            # Loop through scenario
            self.RunScenarios()

            # Wait for Outputs
            self.WaitForOutputs()

        if userStartPoint == UIChoice.HumanTrials:
            # Get correct folder
            repeat = True
            while(repeat):
                self.logger.info("Requesting user input for Query Results folder to perform human tests on...")
                self.queryResultsDirectory = input("*****Musicc Query Results Directory*****\n\nEnter the path to the folder you wish to perform human trials on :\n\n")

                # Checking for valid input
                if (self.queryResultsDirectory != ""):
                    repeat = False

            # Read Query Results Concrete saved from ADS run
            self.queryResultsConcrete = json.load(open(self.queryResultsDirectory + "/QueryResultsConcrete.json")) 

            # Read through test results and build list of scenarioPaths 
            self.GetScenarioPathsFromTestResults()
   
            # Initialise Simulator
            self.InitialiseSimulator()

            # Loop through scenario
            self.RunScenarios(True)

        # Starting the Test and Report Section of the Program 
        if(userStartPoint == UIChoice.ADSRunScenariosAndTests or userStartPoint == UIChoice.ReadInAndRunTests or userStartPoint == UIChoice.HumanTrials):
            self.logger.info("Starting process to run tests...")

            scenarios = []
            # If the User has Specified they want to read Raw Data Files and Produce Test Results and a Report on them, get the Location of the Files
            if(userStartPoint == UIChoice.ReadInAndRunTests):
                self.logger.info("Getting user input for desired read location for predefined data sets")
                repeat = True
                while(repeat):

                    # Getting Local Directory of Data Sets
                    rawDataFileLocation = input("\nPlease Enter The Local Directory Where Your Desired Raw Data Sets Are\n\n")

                    #TODO:Check Validity of Local Directory of Data Sets
                    if(rawDataFileLocation != "Incorrect"):
                        repeat = False
                        outputFileNames = glob.glob(rawDataFileLocation + "\*.txt")
                        for fileName in outputFileNames:
                            simulationDataString = open(fileName + ".txt","rb")
                            rawScenarioData = certitrace_pb2.SimulationRecords()
                            rawScenarioData.ParseFromString(simulationDataString.read())
                            scenarios.append(rawScenarioData)
                        
            else:
                # Read in Raw Data Files
                for fileName in self.outputFileNames:
                    simulationDataString = open(fileName + ".txt","rb")
                    rawScenarioData = certitrace_pb2.SimulationRecords()
                    rawScenarioData.ParseFromString(simulationDataString.read())
                    scenarios.append(rawScenarioData)
                    
            if(userStartPoint == UIChoice.HumanTrials):
                #Run Tests and Store Results
                testResults = RunTests(scenarios,self.queryResultsDirectory,True)
            else:
                #Run Tests and Store Results
                testResults = RunTests(scenarios,self.queryResultsDirectory)
            print("")
            print("")

            if(userStartPoint != UIChoice.HumanTrials):
                #Check for human input required
                humanTrialsNeeded = self.CheckForHumanTrialsRequired(testResults)

                if humanTrialsNeeded == False:
                    #TODO:Generate Report Based on Test Results
                    self.GenerateReport(testResults)
            else:
                # Get all test results from current query
                listOfFiles = os.listdir(self.queryResultsDirectory)
                
                #Filtered List to be populated
                newListOfFiles = []
                
                for file in listOfFiles:
                    if file[0:11] == "TestResults":
                        newListOfFiles.append(file)

                # Display all test result files
                for file in newListOfFiles:
                    if file == "TestResults.txt":
                        print("ADS Test Results : {0}".format(file))
                    else:
                        print("Human Test Results : {0}".format(file))
                # Ask if the required amount of human trials has been forfilled
                repeat = True

                # Getting Local Directory of Data Sets
                while(repeat):
                    self.logger.info("Requesting user input for if they have performed a sufficient number of human trialss...")
                    sufficientTrialsChoice = input("*****Sufficient Amount of Human Tests?*****\n\nEnter the number which is next to your desired choice\n\n\t1 <--- Yes, enough human trials have been performed please compare results and generate report\n\t2 <--- No, not enough human trials have been performed\n\n")

                    # Checking for valid input
                    if (sufficientTrialsChoice == "1" or sufficientTrialsChoice == "2"):
                        repeat = False
                # Compare Results and produce a final Test Results file
                self.CompareResultsAndProduceTestResultsFinal(newListOfFiles)

                # Generate Report
                self.GenerateReport()
    ## Method that queries and then downloads from your scenario database (Example Implementation - Musicc)
    def QueryScenarioDatabase(self):
        # Make API Call to MUSICC
        with MusiccSession(self.configData.url) as session:
            session.login(self.configData.username,self.configData.password)

            self.queryResults= session.query(self.queryString)

            self.logger.info("Response from query : {0}".format(vars(self.queryResults)))
            if self.queryResults.count == 0:
                raise Exception("Query response had no scenarios")

            self.queryUrl = (session.base_url + session.query_url)

            fullPath = session.stream_file(self.queryResults,self.configData.outputDirectory,self.concreteScenariosPerLogical)

            self.queryResultsDirectory = fullPath[0:-4]

        # Unzip the file received from the download
        with zipfile.ZipFile(fullPath, 'r') as zip_ref:
            if os.path.exists(self.queryResultsDirectory):
                print(self.queryResultsDirectory, " Already Exists... using this directory")
            else:
                os.mkdir(self.queryResultsDirectory)
            zip_ref.extractall(self.queryResultsDirectory)
    ## Method that calls the bash script that initialises the simulator (Example Implementation - Carla)
    #
    # @param asyncMode Defines if you want the simulator to run asynchronously or synchronously
    def InitialiseSimulator(self, asyncMode = False):
        SIM_START_TIMEOUT = 2 * 60; # in seconds
        timeout = time.time() + SIM_START_TIMEOUT

        if os.name == 'nt':
            sim_args = ['Simulation-Scripts\initialiseCarla.bat']
            sim_executable = "CarlaUE4-Win64-Shipping.exe"
        else:
            sim_args = ['sh', './Simulation-Scripts/initialiseCarla.sh']
            sim_executable = "CarlaUE4-Linux-Shipping"

        if not asyncMode:
            sim_args.append('-sync')

        carlaServerThread = Thread(target = subprocess.call, args = (sim_args,))
        carlaServerThread.start()
        while time.time() < timeout:
            if (sim_executable in (p.name() for p in psutil.process_iter())):
                print("Found CARLA process")
                time.sleep(10)
                return
        raise Exception("Timeout waiting for simulator to start")

    ## Method that runs all of the scenarios received from scenario database (Example Implementation - CARLA -> scenario runner)
    #
    # @param asyncMode In our case we could define if the client (that connects to the server simulator)would run asynchronously or synchronously
    def RunScenarios(self, asyncMode = False):
        scenarioNum = 0
        for result in self.queryResultsConcrete:
            # Run scenario runner
            print("running scenario runner")
            # Use for musicc scenario

            musiccID = result["id"]
            concreteScenarioIdentifier = os.path.basename(self.scenarioPaths[scenarioNum])

            scenarioDir = self.scenarioPaths[scenarioNum] + ".xosc"

            ConvertMusiccOSCToScenarioRunner(scenarioDir)

            # Initialise Output file
            scenarioStartTime = datetime.now().strftime("%Y_%m_%d_(%H-%M-%S)")
            self.outputFileNames.append(self.queryResultsDirectory)

            # Make Results Folder
            try:
                os.mkdir(self.queryResultsDirectory + "/" + musiccID + "/")
            except Exception as e:
                print(e)
                pass
            try:
                os.mkdir(self.queryResultsDirectory + "/" + musiccID + "/Results-" + concreteScenarioIdentifier + "/")
            except Exception as e:
                print(e)
                pass
            self.outputFileNames[scenarioNum] +=  "/" + musiccID + "/Results-" + concreteScenarioIdentifier + "/"

            self.outputFileNames[scenarioNum] += scenarioStartTime
            self.outputFileNames[scenarioNum] += concreteScenarioIdentifier

            scenario_runner.main(["--openscenario", scenarioDir, "--sync", "--timeout", "20"], result, self.queryString, self.queryUrl, concreteScenarioIdentifier, self.certicavCommit, self.configData.organisation, self.outputFileNames[scenarioNum], asyncMode)

            scenarioNum += 1

    ## Method that waits for the outputs when calling your scenarios
    def WaitForOutputs(self):
        outputCounter= 0
        repeat = True
        while(repeat):
            # Get all files in output directory
            while not os.path.exists(self.outputFileNames[outputCounter] + ".txt"):
                time.sleep(1)
            outputCounter +=1

            # If outputs is equal to the amount of tests we wanted to run then move on
            if (outputCounter == len(self.scenarioPaths)):
                self.logger.info("All Scenarios completed and Output files created")
                print("All Scenarios completed and Output files created")
                repeat = False
            else:
                if(datetime.now() > self.timeoutTime):
                    self.logger.error("Timeout - Simulator took too long to execute scenario {}".format(outputCounter + 1))
                    raise Exception ("Timeout - Simulator took too long to execute scenario {}".format(outputCounter + 1))
                # If not output how many have been done
                print("{0}/{1} Scenarios Completed".format(outputCounter,len(self.scenarioPaths)), end="\r")
    ## Generates a html report based on the test results you give
    #
    # @param testResults Passes in the test results from this run, if empty then reads finalTestResults from file
    def GenerateReport(self,testResults = None):

        startHtmlStr = """
        <html lang="en">
        <head>
        <meta charset="utf-8">

        <title>Report</title>
        <meta name="description" content="Report">
        <meta name="author" content="Report">
        <style>
        
        body{
            background-color: aliceblue;
        }
        h1{
            color: #303D4B;
        }
        div{
            margin: 10px; 
            margin-left: 20px;
            padding: 5px;
            padding-left: 20px; 
            background-color:#303D4B; 
            color:aliceblue; 
            border-radius: 5px;
        }
        table{
            color:aliceblue;
            padding :10px
        }
        tr{
            padding:5px;
        }
        td{
            padding:5px;
        }
        button{
                background-color: aliceblue;
                font-weight: bold;
                margin:10px;
                margin-left: 40px; 
                padding:10px;
                border-radius: 12px;
        }
        .GroupingScenario{
            transition: 0.5s ease-in-out;
            overflow:hidden;
        }
        .hide {
        height: 0;
        }
        </style>

        <script>
            function slide(id) {
                var elem = document.getElementById(id);
                var content = elem.nextElementSibling;
                content.classList.toggle('hide');
            }
        </script>
        </head>
        
        <body>
        <h1>Test Report</h1>
        """

        endHtmlStr = """
        </body>
        </html>
        """
        htmlStr = ""          

        # If test results are passed to the method then use this 
        if testResults != None:        
            logicalScenarioCount = self.queryResults.count
            concreteScenarioCount = len(self.queryResultsConcrete)
            collapsibleItr = 0
            # Construct HTML
            htmlStr += startHtmlStr
            htmlStr += """
            <div>
            <h3>Number of logical scenarios tested : """ + str(logicalScenarioCount) + """
            <h3>Number of concrete scenarios tested : """ + str(concreteScenarioCount) + """
            </div>
            """
            for testType in testResults.testTypes:
                htmlStr += """
                <div class="Test Type">
                <h2>Test type : """ + testType.name + """ </h2>
                <h4>Overall Result : """ + testType.result + """ </h4>
                <div>
                """
                for test in testType.tests:
                    collapsibleItr += 1
                    htmlStr += """
                    <div class="Test">
                    <h3>Test Name : """ + test.name + """</h3>
                    <h4>Result : """ + test.result +  """</h4>
                    <button type="button" id="collapsible""" + str(collapsibleItr) + """\" onclick="slide(id)">Detailed Results</button>
                    """
                    for groupingScenario in test.groupingScenarios:
                        htmlStr += """
                        <div class="GroupingScenario hide">
                        <h5>""" + groupingScenario.name + """
                        <table>
                        <tr>
                        <td>Concrete Scenario ID</td>
                        <td>Result</td>
                        <td>Score</td>
                        <td>Message</td>
                        </tr>
                        """
                        for scenario in groupingScenario.scenarios:
                            htmlStr += """
                            <tr>
                            <td> """ + str(scenario.name) + """ </td>
                            <td> """ + scenario.result + """ </td>
                            <td> """ + scenario.score + """ </td>
                            <td> """ + scenario.message + """ </td>
                            </tr>
                            """
                        htmlStr+= """
                        </table>
                        </div>
                        """
                    htmlStr+= """
                    </div>
                    """
                htmlStr += """
                </div>
                </div>
                """
        else:
            # If test results are not passed to the method then use this
            testResultsFile = open(self.queryResultsDirectory + "/TestResultsFinal.txt")
            testResults = json.loads(testResultsFile.read())
            logicalScenarioCount = len(testResults['testTypes'][0]['tests'][0]['groupingScenarios'])
            concreteScenarioCount = len(self.queryResultsConcrete)
            collapsibleItr = 0
            # Construct HTML
            htmlStr += startHtmlStr
            htmlStr += """
            <div>
            <h3>Number of logical scenarios tested : """ + str(logicalScenarioCount) + """
            <h3>Number of concrete scenarios tested : """ + str(concreteScenarioCount) + """
            </div>
            """
            for testType in testResults['testTypes']:
                htmlStr += """
                <div class="Test Type">
                <h2>Test type : """ + testType['name'] + """ </h2>
                <h4>Overall Result : """ + testType['result'] + """ </h4>
                <div>
                """
                for test in testType['tests']:
                    collapsibleItr += 1
                    htmlStr += """
                    <div class="Test">
                    <h3>Test Name : """ + test['name'] + """</h3>
                    <h4>Result : """ + test['result'] +  """</h4>
                    <button type="button" id="collapsible""" + str(collapsibleItr) + """\" onclick="slide(id)">Detailed Results</button>
                    """
                    for groupingScenario in test['groupingScenarios']:
                        htmlStr += """
                        <div class="GroupingScenario hide">
                        <h5>""" + groupingScenario['name'] + """
                        <table>
                        <tr>
                        <td>Concrete Scenario ID</td>
                        <td>Result</td>
                        <td>Score</td>
                        <td>Message</td>
                        </tr>
                        """
                        for scenario in groupingScenario['scenarios']:
                            htmlStr += """
                            <tr>
                            <td> """ + str(scenario['name']) + """ </td>
                            <td> """ + scenario['result'] + """ </td>
                            <td> """ + scenario['score'] + """ </td>
                            <td> """ + scenario['message'] + """ </td>
                            </tr>
                            """
                        htmlStr+= """
                        </table>
                        </div>
                        """
                    htmlStr+= """
                    </div>
                    """
                htmlStr += """
                </div>
                </div>
                """

        htmlStr += endHtmlStr 
        htmlFile= open(self.queryResultsDirectory + "/TestReport.html","w")
        htmlFile.write(htmlStr)
        htmlFile.close()
        print("Saved Test Report to ", self.queryResultsDirectory + "/TestReport.html")
    ## Uses self.queryResults to get the paths to the concrete scenarios
    def GetScenarioPaths(self):
        for result in self.queryResults.results:
            if (result['metadata']['ScenarioType'].lower() == "concrete") or (self.concreteScenariosPerLogical == 1):
                self.scenarioPaths.append(self.queryResultsDirectory + "/" + result['id'] + "/" + result['metadata']['OpenScenario_ID'])
            else:
                for concreteItr in range(0,self.concreteScenariosPerLogical):
                    self.scenarioPaths.append(self.queryResultsDirectory + "/" + result['id'] + "/" + result['metadata']['OpenScenario_ID'] + "_" + str(concreteItr))
    ## Uses the test results inside the self.queryResultsDirectory to get the paths to the concerte scenarios
    def GetScenarioPathsFromTestResults(self):
        testResultsFile = open(self.queryResultsDirectory + "/TestResults.txt")
        testResults = json.loads(testResultsFile.read())
        for testType in testResults['testTypes']:
            for test in testType['tests']:
                for groupingScenario in test['groupingScenarios']:
                    for scenario in groupingScenario['scenarios']:
                        if scenario['result'] == "Human Trials Required":    
                            # Find relating concrete result
                            result = None
                            for tryResult in self.queryResultsConcrete:
                                if tryResult['metadata']['OpenScenario_ID'] == groupingScenario['name']:
                                    result = tryResult
                                    break
                            if result == None:
                                raise Exception("Result not found in QueryResultConcrete when using Scenario Path to identify")
                            
                            tempScenarioPath = self.queryResultsDirectory + "/" + result['metadata']['MUSICC_ID'] + "/" + scenario['name'] 
                            if tempScenarioPath not in self.scenarioPaths:
                                self.scenarioPaths.append(tempScenarioPath)  
        if len(self.scenarioPaths) == 0:
            raise Exception("None of the scenarios in the selected results need human trials")
    ## Converting the self.queryResults into having a query result for each concrete scenarios and saving that to self.queryResultsConcrete
    def QueryResultsToConcrete(self):
        for result in self.queryResults.results:
            if result['metadata']['ScenarioType'].lower() == "concrete":
                self.queryResultsConcrete.append(result)
            else:
                for concreteItr in range(0,self.concreteScenariosPerLogical):
                    self.queryResultsConcrete.append(result)
        
        # Write to file
        file = open(self.queryResultsDirectory + "/QueryResultsConcrete.json","w")
        file.write(json.dumps(self.queryResultsConcrete))
        file.close()
    ## Check the test results to see if there are any results of "Human Trials Required"
    #
    # @param testResults The test results that you want checking for "Human Trials Required" result
    def CheckForHumanTrialsRequired(self, testResults):
        humanTrialsRequired = False
        for testType in testResults.testTypes:
            if testType.result == "Non-Complaint":
                return False
            if testType.result == "Human Trials Required":
                humanTrialsRequired = True
        if humanTrialsRequired:
            print("Report will not be generated because one of more of the scenarios require human trials")
        return humanTrialsRequired
    ## Compares the test results of the human and ads trials... creating a final test results with comparison messages in the scenarios message fields
    #
    # @param files A List of filenames that are pointers to the Human and ADS test results that are then used for comparison and finalTestResult generation
    def CompareResultsAndProduceTestResultsFinal(self,files):
        HumanResults = []
        for file in files:
            if file == "TestResults.txt":
                ADSResultsFile = open(self.queryResultsDirectory + "/TestResults.txt")
                ADSResults = json.loads(ADSResultsFile.read())
                ADSResultsFile.close()
            else:
                HumanResultsFile = open(self.queryResultsDirectory + "/" + file)
                HumanResults.append(json.loads(HumanResultsFile.read()))
                HumanResultsFile.close()

        finalResult = ADSResults

        for testType in ADSResults['testTypes']:
            for test in testType['tests']:
                for groupingScenario in test['groupingScenarios']:
                    for scenario in groupingScenario['scenarios']:
                        if scenario['result'] == "Human Trials Required":
                            highestHumanScore = 0
                            # find human result relating to the same concrete scenario
                            for humanResult in HumanResults:
                                for humanTestType in humanResult['testTypes']:
                                    if humanTestType['name'] == testType['name']:
                                        for humanTest in humanTestType['tests']:
                                            if humanTest['name'] == test['name']:
                                                for humanGroupingScenario in humanTest['groupingScenarios']:
                                                    if humanGroupingScenario['name'] == groupingScenario['name']:
                                                        for humanScenario in humanGroupingScenario['scenarios']:
                                                            if humanScenario['name'] == scenario['name']:
                                                                if float(humanScenario['score']) > highestHumanScore:
                                                                    highestHumanScore = float(humanScenario['score'])
        
                            #Change final Results 
                            for finalTestType in finalResult['testTypes']:
                                if finalTestType['name'] == testType['name']:
                                    for finalTest in finalTestType['tests']:
                                        if finalTest['name'] == test['name']:
                                            for finalGroupingScenario in finalTest['groupingScenarios']:
                                                if finalGroupingScenario['name'] == groupingScenario['name']:
                                                    for finalScenario in finalGroupingScenario['scenarios']:
                                                        if finalScenario['name'] == scenario['name']:
                                                            #Found same scenario... check if ADS performed better than Human
                                                            if float(finalScenario['score']) > highestHumanScore:
                                                                    #found final scenario
                                                                    finalTestType['result'] = "Non-Compliant"
                                                                    finalTest['result'] = "Non-Compliant"
                                                                    finalGroupingScenario['result'] = "Non-Compliant"
                                                                    finalScenario['result'] = "Non-Compliant"
                                                                    finalScenario['message'] = "Worst scoring human trials for this scenario had a severity score of {0} compared to the ADS score of {1}".format(highestHumanScore,finalScenario['score'])
                                                            else:
                                                                if(finalTestType['result'] == "Human Trials Required"):
                                                                    finalTestType['result'] = "Pass"
                                                                if(finalTest['result'] == "Human Trials Required"):
                                                                    finalTest['result'] = "Non-Compliant"
                                                                if(finalGroupingScenario['result'] == "Human Trials Required"):
                                                                    finalGroupingScenario['result'] = "Non-Compliant"
                                                                finalScenario['result'] = "Pass"
                                                                finalScenario['message'] = "Worst scoring human trials for this scenario had a severity score of {0} compared to the ADS score of {1}".format(highestHumanScore,finalScenario['score'])



        finalResultFile = open(self.queryResultsDirectory + "/TestResultsFinal.txt","w")
        finalResultFile.write(json.dumps(finalResult))
        finalResultFile.close()
## Initialises MyLogger, ConfigData and CertiCAV classes then calls CertiCAV.UI() 
def Main():
    try:
        # Erasing all in log file before use
        loggingFile = open("CertiCAV.log","w")
        loggingFile.truncate(0)
        loggingFile.close()
        
        # Setting up log file
        logging.setLoggerClass(MyLogger)
        logging.basicConfig(filename="CertiCAV.log", level=logging.INFO)
        logger = logging.getLogger('CertiCAV-Logger')

        logger.info("Application Starting...")

        # Get Config File input
        with open("config.json") as jsonFile:
            configData = ConfigData(json.load(jsonFile))
            logger.info("Config data read in : {}".format(vars(configData)))

        certicav = CertiCAV(logger, configData)

        certicav.UI()
    except Exception as e: 
        traceback.print_exc()

if __name__ == "__main__":
    Main()
    

